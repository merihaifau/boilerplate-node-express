require('dotenv').config();

const express = require('express');
const app = express();
const port = process.env.PORT;

if (!port) {
    console.error('PORT is missing from .env file');
    process.exit(-1);
}

app.get('/', (req, res) => res.send('Hello World!'));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
